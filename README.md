# fishfeeder


## Version 1.0.0
LED Routine - White (255,255,255), Purple (255, 0, 255), Aquamarine (0, 255, 255)


## About
A simple project for DIY automatic fish feeder with option for manual feeding. 3d printed files and code is included. The amount of food is regulated by changing the size of the hole that us used.

## Hardware

* Arduino nano or compatable
* Push button
* RGB Led
* RTC ds1307 or compatable
* Continous rotating servo ds04 or comaptable

## Libraries

* RTClib.h
* Servo.h
* Adafruit_NeoPixel.h

## 3D files

All files are ready for printing. They can be found inside ./models/ForPrint. FishFeeder-Lid FishFeeder-Feeder-body must be printed with scalin 102%; Anycubiv vyper with standart PLA was used.

* FishFeeder-box - main body of the feeder
* FishFeeder-box-Lid - lid for the main body
* FishFeeder-Feeder-body - food container
* FishFeeder-Lid - lid for the food container
* FishFeeder-Funel - funel for inserting food inside the food container.
