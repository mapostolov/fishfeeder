#version 3.7; // 3.6
global_settings { assumed_gamma 1.0 }
#default { finish { ambient 0.2 diffuse 0.9 } }
#default { pigment { rgb <0.800, 0.800, 0.800> } }

//------------------------------------------
#include "colors.inc"
#include "textures.inc"

//------------------------------------------
#include "FishFeeder_textures.inc"
#include "FishFeeder_meshes.inc"

//------------------------------------------
// Camera ----------------------------------
#declare CamUp = < 0, 0, 147.55>;
#declare CamRight = <196.73, 0, 0>;
#declare CamRotation = <-23.995462558639858, -0.35499537700206246, -29.089819125367097>;
#declare CamPosition = <-469.5071716308594, -789.040283203125, 427.7904052734375>;
camera {
	orthographic
	location <0, 0, 0>
	direction <0, 1, 0>
	up CamUp
	right CamRight
	rotate CamRotation
	translate CamPosition
}

// FreeCAD Light -------------------------------------
light_source { CamPosition color rgb <0.5, 0.5, 0.5> }

// Background ------------------------------

polygon {
	5, <-98.36654663085938, -73.77490997314453>, <-98.36654663085938, 73.77490997314453>, <98.36654663085938, 73.77490997314453>, <98.36654663085938, -73.77490997314453>, <-98.36654663085938, -73.77490997314453>
	pigment {
		gradient y
		color_map {
			[ 0.00  color rgb<0.659, 0.659, 0.659> ]
			[ 0.05  color rgb<0.659, 0.659, 0.659> ]
			[ 0.95  color rgb<0.431, 0.431, 0.431> ]
			[ 1.00  color rgb<0.431, 0.431, 0.431> ]
		}
		scale <1,147.54981994628906,1>
		translate <0,-73.77490997314453,0>
	}
	finish { ambient 1 diffuse 0 }
	rotate <66.00453744136014, -0.35499537700206246, -29.089819125367097>
	translate <-469.5071716308594, -789.040283203125, 427.7904052734375>
	translate <44636.5088224411, 79711.2762928009, -40665.65036773682>
}
sky_sphere {
	pigment {
		gradient z
		color_map {
			[ 0.00  color rgb<0.659, 0.659, 0.659> ]
			[ 0.30  color rgb<0.659, 0.659, 0.659> ]
			[ 0.70  color rgb<0.431, 0.431, 0.431> ]
			[ 1.00  color rgb<0.431, 0.431, 0.431> ]
		}
		scale 2
		translate -1
		rotate<-23.995462558639858, -0.35499537700206246, -29.089819125367097>
	}
}

//------------------------------------------

#include "FishFeeder_user.inc"

// Objects in Scene ------------------------

//----- Model -----
union {

	//----- LCS_Origin -----
	//----- Constraints -----
	//----- Variables -----
	//----- Configurations -----
}

//----- X_Axis002 -----
//----- Y_Axis002 -----
//----- Z_Axis002 -----
//----- XY_Plane002 -----
//----- XZ_Plane002 -----
//----- YZ_Plane002 -----
//----- DS04_NFC -----
union {

	//----- DS04_NFC_Body -----
	object { DS04_NFC_Body_mesh
			pigment { color rgb <0.753, 0.753, 0.753> }
	
	}
	
	//----- Coupling -----
	object { Coupling_mesh
			pigment { color rgb <0.753, 0.753, 0.753> }
	
	}
	
	rotate <89.99999999999999, -90.0, 0.0>
	translate <-65.96000000000001, 6.4e-15, 28.620000000000015>
}

//----- X_Axis -----
//----- Y_Axis -----
//----- Z_Axis -----
//----- XY_Plane -----
//----- XZ_Plane -----
//----- YZ_Plane -----
//----- TinyRTC_1307_cp -----
object { TinyRTC_1307_cp_mesh
}

//----- Arduino_Nano_STEP -----
object { Arduino_Nano_STEP_mesh
}

//----- X_Axis003 -----
//----- Y_Axis003 -----
//----- Z_Axis003 -----
//----- XY_Plane003 -----
//----- XZ_Plane003 -----
//----- YZ_Plane003 -----
//----- X_Axis004 -----
//----- Y_Axis004 -----
//----- Z_Axis004 -----
//----- XY_Plane004 -----
//----- XZ_Plane004 -----
//----- YZ_Plane004 -----
//----- X_Axis005 -----
//----- Y_Axis005 -----
//----- Z_Axis005 -----
//----- XY_Plane005 -----
//----- XZ_Plane005 -----
//----- YZ_Plane005 -----
//----- X_Axis006 -----
//----- Y_Axis006 -----
//----- Z_Axis006 -----
//----- XY_Plane006 -----
//----- XZ_Plane006 -----
//----- YZ_Plane006 -----
//----- X_Axis007 -----
//----- Y_Axis007 -----
//----- Z_Axis007 -----
//----- XY_Plane007 -----
//----- XZ_Plane007 -----
//----- YZ_Plane007 -----
//----- X_Axis008 -----
//----- Y_Axis008 -----
//----- Z_Axis008 -----
//----- XY_Plane008 -----
//----- XZ_Plane008 -----
//----- YZ_Plane008 -----
//----- X_Axis009 -----
//----- Y_Axis009 -----
//----- Z_Axis009 -----
//----- XY_Plane009 -----
//----- XZ_Plane009 -----
//----- YZ_Plane009 -----
//----- X_Axis010 -----
//----- Y_Axis010 -----
//----- Z_Axis010 -----
//----- XY_Plane010 -----
//----- XZ_Plane010 -----
//----- YZ_Plane010 -----
//----- X_Axis011 -----
//----- Y_Axis011 -----
//----- Z_Axis011 -----
//----- XY_Plane011 -----
//----- XZ_Plane011 -----
//----- YZ_Plane011 -----
//----- _1602_LCD -----
union {

	//----- PCB -----
	object { PCB_mesh
	}
	
	//----- Mounting_Screw_Ring -----
	object { Mounting_Screw_Ring_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Mounting_Screw_Ring_mir -----
	object { Mounting_Screw_Ring_mir_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Mounting_Screw_Ring_mir001 -----
	object { Mounting_Screw_Ring_mir001_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Mounting_Screw_Ring001 -----
	object { Mounting_Screw_Ring001_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating -----
	object { Wire_Connector_Plating_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating001 -----
	object { Wire_Connector_Plating001_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating002 -----
	object { Wire_Connector_Plating002_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating003 -----
	object { Wire_Connector_Plating003_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating004 -----
	object { Wire_Connector_Plating004_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating005 -----
	object { Wire_Connector_Plating005_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating006 -----
	object { Wire_Connector_Plating006_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating007 -----
	object { Wire_Connector_Plating007_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating008 -----
	object { Wire_Connector_Plating008_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating009 -----
	object { Wire_Connector_Plating009_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating010 -----
	object { Wire_Connector_Plating010_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating011 -----
	object { Wire_Connector_Plating011_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating012 -----
	object { Wire_Connector_Plating012_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating013 -----
	object { Wire_Connector_Plating013_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating014 -----
	object { Wire_Connector_Plating014_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating015 -----
	object { Wire_Connector_Plating015_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- SMD_Resistor_1206 -----
	union {
	
		//----- SMD_R_1206_Body -----
		object { SMD_R_1206_Body_mesh
				pigment { color rgb <0.200, 0.200, 0.200> }
		
		}
		
		//----- SMD_R_1206_Connector -----
		object { SMD_R_1206_Connector_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		//----- SMD_R_1206_Connector001 -----
		object { SMD_R_1206_Connector001_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		translate <14.0262506823256, 0.6, 14.65>
	}
	
	//----- LCD_Conductive_Band -----
	object { LCD_Conductive_Band_mesh
			pigment { color rgb <0.702, 0.212, 0.212> }
	
	}
	
	//----- LCD_Screen -----
	object { LCD_Screen_mesh
	}
	
	//----- LCD_Glass -----
	object { LCD_Glass_mesh
	}
	
	//----- SMD_Resistor_1206001 -----
	union {
	
		//----- SMD_R_1206_Body001 -----
		object { SMD_R_1206_Body001_mesh
				pigment { color rgb <0.200, 0.200, 0.200> }
		
		}
		
		//----- SMD_R_1206_Connector002 -----
		object { SMD_R_1206_Connector002_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		//----- SMD_R_1206_Connector003 -----
		object { SMD_R_1206_Connector003_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		rotate <180.0, 7.016709298534876e-15, 180.0>
		translate <-31.35, 0.6, 4.88>
	}
	
	//----- LCD_Conductive_Band001 -----
	object { LCD_Conductive_Band001_mesh
			pigment { color rgb <0.702, 0.212, 0.212> }
	
	}
	
	//----- SMD_Resistor_1206002 -----
	union {
	
		//----- SMD_R_1206_Body002 -----
		object { SMD_R_1206_Body002_mesh
				pigment { color rgb <0.200, 0.200, 0.200> }
		
		}
		
		//----- SMD_R_1206_Connector004 -----
		object { SMD_R_1206_Connector004_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		//----- SMD_R_1206_Connector005 -----
		object { SMD_R_1206_Connector005_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		rotate <180.0, 7.016709298534876e-15, 180.0>
		translate <14.0262506823256, 0.6, 9.95>
	}
	
	//----- SMD_Resistor_1206003 -----
	union {
	
		//----- SMD_R_1206_Body003 -----
		object { SMD_R_1206_Body003_mesh
				pigment { color rgb <0.200, 0.200, 0.200> }
		
		}
		
		//----- SMD_R_1206_Connector006 -----
		object { SMD_R_1206_Connector006_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		//----- SMD_R_1206_Connector007 -----
		object { SMD_R_1206_Connector007_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		rotate <180.0, 7.016709298534876e-15, 180.0>
		translate <-18.35, 0.6, -3.76>
	}
	
	//----- SMD_Resistor_1206004 -----
	union {
	
		//----- SMD_R_1206_Body004 -----
		object { SMD_R_1206_Body004_mesh
				pigment { color rgb <0.200, 0.200, 0.200> }
		
		}
		
		//----- SMD_R_1206_Connector008 -----
		object { SMD_R_1206_Connector008_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		//----- SMD_R_1206_Connector009 -----
		object { SMD_R_1206_Connector009_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		translate <-31.25, 0.6, 2.62>
	}
	
	//----- SMD_Resistor_1206005 -----
	union {
	
		//----- SMD_R_1206_Body005 -----
		object { SMD_R_1206_Body005_mesh
				pigment { color rgb <0.200, 0.200, 0.200> }
		
		}
		
		//----- SMD_R_1206_Connector010 -----
		object { SMD_R_1206_Connector010_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		//----- SMD_R_1206_Connector011 -----
		object { SMD_R_1206_Connector011_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		translate <-31.25, 0.6, -3.86>
	}
	
	//----- SMD_Resistor_1206006 -----
	union {
	
		//----- SMD_R_1206_Body006 -----
		object { SMD_R_1206_Body006_mesh
				pigment { color rgb <0.200, 0.200, 0.200> }
		
		}
		
		//----- SMD_R_1206_Connector012 -----
		object { SMD_R_1206_Connector012_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		//----- SMD_R_1206_Connector013 -----
		object { SMD_R_1206_Connector013_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		translate <-31.25, 0.6, 0.459999999999998>
	}
	
	//----- SMD_Resistor_1206007 -----
	union {
	
		//----- SMD_R_1206_Body007 -----
		object { SMD_R_1206_Body007_mesh
				pigment { color rgb <0.200, 0.200, 0.200> }
		
		}
		
		//----- SMD_R_1206_Connector014 -----
		object { SMD_R_1206_Connector014_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		//----- SMD_R_1206_Connector015 -----
		object { SMD_R_1206_Connector015_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		translate <-31.25, 0.6, -1.7>
	}
	
	//----- SMD_Resistor_1206008 -----
	union {
	
		//----- SMD_R_1206_Body008 -----
		object { SMD_R_1206_Body008_mesh
				pigment { color rgb <0.200, 0.200, 0.200> }
		
		}
		
		//----- SMD_R_1206_Connector016 -----
		object { SMD_R_1206_Connector016_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		//----- SMD_R_1206_Connector017 -----
		object { SMD_R_1206_Connector017_mesh
				pigment { color rgb <0.820, 0.820, 0.820> }
		
		}
		
		translate <-31.25, 0.6, -6.02>
	}
	
	//----- Mounting_Screw_Ring002 -----
	object { Mounting_Screw_Ring002_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Mounting_Screw_Ring003 -----
	object { Mounting_Screw_Ring003_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Mounting_Screw_Ring004 -----
	object { Mounting_Screw_Ring004_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Mounting_Screw_Ring005 -----
	object { Mounting_Screw_Ring005_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating016 -----
	object { Wire_Connector_Plating016_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Round_Chip -----
	object { Round_Chip_mesh
			pigment { color rgb <0.000, 0.000, 0.000> }
	
	}
	
	//----- LCD_Frame -----
	object { LCD_Frame_mesh
			pigment { color rgb <0.200, 0.200, 0.200> }
	
	}
	
	//----- Wire_Connector_Plating017 -----
	object { Wire_Connector_Plating017_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating018 -----
	object { Wire_Connector_Plating018_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating019 -----
	object { Wire_Connector_Plating019_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating020 -----
	object { Wire_Connector_Plating020_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating021 -----
	object { Wire_Connector_Plating021_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating022 -----
	object { Wire_Connector_Plating022_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating023 -----
	object { Wire_Connector_Plating023_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating024 -----
	object { Wire_Connector_Plating024_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Round_Chip001 -----
	object { Round_Chip001_mesh
			pigment { color rgb <0.000, 0.000, 0.000> }
	
	}
	
	//----- Wire_Connector_Plating025 -----
	object { Wire_Connector_Plating025_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating026 -----
	object { Wire_Connector_Plating026_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating027 -----
	object { Wire_Connector_Plating027_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating028 -----
	object { Wire_Connector_Plating028_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating029 -----
	object { Wire_Connector_Plating029_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating030 -----
	object { Wire_Connector_Plating030_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	//----- Wire_Connector_Plating031 -----
	object { Wire_Connector_Plating031_mesh
			pigment { color rgb <0.871, 0.631, 0.173> }
	
	}
	
	translate <0.0, -15.359999999999975, 31.83000000000001>
}

//----- X_Axis012 -----
//----- Y_Axis012 -----
//----- Z_Axis012 -----
//----- XY_Plane012 -----
//----- XZ_Plane012 -----
//----- YZ_Plane012 -----
//----- Box -----
object { Box_mesh
}

//----- X_Axis013 -----
//----- Y_Axis013 -----
//----- Z_Axis013 -----
//----- XY_Plane013 -----
//----- XZ_Plane013 -----
//----- YZ_Plane013 -----
//----- Lid -----
object { Lid_mesh
}

//----- X_Axis014 -----
//----- Y_Axis014 -----
//----- Z_Axis014 -----
//----- XY_Plane014 -----
//----- XZ_Plane014 -----
//----- YZ_Plane014 -----
//----- X_Axis015 -----
//----- Y_Axis015 -----
//----- Z_Axis015 -----
//----- XY_Plane015 -----
//----- XZ_Plane015 -----
//----- YZ_Plane015 -----
//----- Feeder_body -----
difference {

	//----- Feeder_Body -----
	object { Feeder_Body_mesh
	}
	
	//----- Coupling001 -----
	object { Coupling001_mesh
			pigment { color rgb <0.753, 0.753, 0.753> }
	
	}
	
	rotate <360.0, -90.0, 0.0>
	translate <-65.80000000000003, 0.0, 38.21000000000001>
}

//----- Feeder_lid -----
object { Feeder_lid_mesh
}

//----- X_Axis016 -----
//----- Y_Axis016 -----
//----- Z_Axis016 -----
//----- XY_Plane016 -----
//----- XZ_Plane016 -----
//----- YZ_Plane016 -----
//----- PointLight -----
light_source { <0, 0, 0>
	color rgb<1.0, 1.0, 1.0>
	rotate <-45.00000000000001, -0.0, 0.0>
	translate <0.0, -206.68, 0.0>
}
