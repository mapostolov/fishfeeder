#include "RTClib.h"
#include <Servo.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h>     // Required for 16 MHz Adafruit Trinket
#endif


#define LOOP_DELAY                10000
#define FEEDING_DELAY             1400   
#define BUTTON_PIN                3          // Pin for RGB 
#define LED_COUNT                 1          // Pin for RGB LED
#define LED_PIN                   2          // Pin for RGB LED
#define BRIGHTNESS                100        // Pin for RGB LEDu
#define SERVO_MOVEMENT_SPEED      2400
#define SERVO_PIN                 8


Servo myservo;
Adafruit_NeoPixel led(LED_COUNT, LED_PIN, NEO_GRBW + NEO_KHZ800);
RTC_DS1307 rtc;


int nextDayForFeeding =-1;
bool foodWasGiven = false;
const int HOUR_TO_FEED = 10;


void setup () {    
  while (!Serial); // for Leonardo/Micro/Zero
  pinMode(LED_PIN, OUTPUT);  
  pinMode(BUTTON_PIN, INPUT); 
  led.begin();
  led.setBrightness(BRIGHTNESS);
  setLed(255,255,255);
  Serial.begin(57600);
  if (! rtc.begin()) {  
    Serial.println("Couldn't find RTC");
    setLed(255,0,0);
    while (1);
  }
  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    setLed(255,0,0);
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  versionView();
}

// LED Routine for version 1.0.0 - White (255,255,255), Purple (255, 0, 255), Aquamarine (0, 255, 255)
void versionView(){
  setLed(255,255,255);
  delay(1000);
  setLed(255,0,255);
  delay(1000);
  setLed(0,255,255);
  delay(1000);
}

void setLed(int r, int g, int b){
  led.clear();                                    // Reset all pixels  
  led.setPixelColor(0, led.Color(r, g, b));       // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
  led.show(); 
}


void feedTheFish(int currDay){  
  setLed(0,0,255);
  myservo.attach(SERVO_PIN);
  myservo.writeMicroseconds(SERVO_MOVEMENT_SPEED); //SPEED AND DIRECTION
  delay(FEEDING_DELAY);
  myservo.detach();
  foodWasGiven = true;
  nextDayForFeeding = nextDayForFeeding+1;
}

void loop () {
   setLed(0,255,0);
   int buttonState = digitalRead(BUTTON_PIN);
   
   DateTime now = rtc.now();
   
   if(nextDayForFeeding == -1 || (nextDayForFeeding > 6 && now.dayOfTheWeek() == 0) || nextDayForFeeding < now.dayOfTheWeek()){
     nextDayForFeeding = now.dayOfTheWeek();
   }

   if(nextDayForFeeding == now.dayOfTheWeek()){
     foodWasGiven = false;
   }
     
  if(HOUR_TO_FEED ==  now.hour() && foodWasGiven == false){
     feedTheFish(now.dayOfTheWeek());
   }
  
  if (buttonState == HIGH) {
    nextDayForFeeding = now.dayOfTheWeek();
    feedTheFish(now.dayOfTheWeek());
  }
  delay(LOOP_DELAY);
}
